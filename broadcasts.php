<?php
require 'checkAuth.php';
require 'db_connect.php';
// require '../twilio-php-master/Twilio/autoload.php';

require 'apiServer/vendor/autoload.php';
use Twilio\Rest\Client;
use Mailgun\Mailgun;
// mailgun client setup
$mgClient = Mailgun::create('key-b097370bf2735145ecd6105b49b607d7', 'https://api.mailgun.net/v3');
$domain = "mg.safekeepingapp.com";
$counter = 1;
$emailCounter = 0;
$emails = array();
$uniqueEmails = array();
$recipientVars = "{";
$smsCounter = 0;
$batch = 0;
        
// twilio client setup
$auth_token = '8be15ef49cea972db8618cf596aa7b45';
$account_sid = 'AC27651d18c4f5a2c5c8ef98a4929f4c0f';
$twilio_number = "+13173448895";
$messageSid = 'MG243d43d2ab0812ead58a2d7650d6e3f0';
$serviceSid = 'IS44b5855919c271d760c236f3f83d3b72';
$client = new Client($account_sid, $auth_token);

$subject = 'facilityAlerts';

$facId = "";
if(isset($_GET['orgUuid'])){
    $orgUuid = trim(mysqli_real_escape_string($link, $_GET['orgUuid']));
    if(isset($_GET['facId'])){
        $facId = trim(mysqli_real_escape_string($link, $_GET['facId']));
    }
   
} else {
    $orgUuid = trim(mysqli_real_escape_string($link, $_POST['orgUuid']));
    if(isset($_POST['facId'])){
        $facId = trim(mysqli_real_escape_string($link, $_POST['facId']));
    }
}

// find authorization for this user
$authQuery = "SELECT * FROM user WHERE uid = '$skuser' AND (type = 'admin' OR type = 'orgAdmin') AND orgUuid = '$orgUuid'";
$authResult = mysqli_query($link, $authQuery);
if(mysqli_num_rows($authResult) !=  1) {
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized\"}");
}

if(isset($_GET['orgUuid'])){
    $authRow = mysqli_fetch_assoc($authResult);
    if(strlen($facId) > 0){
        $alertQuery = "SELECT * FROM
            facilityAlerts 
            WHERE orgUuid = '$orgUuid' 
            AND facility = $facId  
            ORDER BY sent DESC";
    } else {
        $alertQuery = "SELECT * FROM 
            facilityAlerts 
            WHERE orgUuid = '$orgUuid' 
            ORDER BY sent DESC";
    }
    $result = mysqli_query($link, $alertQuery);
    $json = [];
    while($row = mysqli_fetch_assoc($result)) {
        if(strlen($row['message']) > 0){
            $emailCount = 0;
            $smsCount = 0;
            $pushCount = 0;
            $recipients = json_decode($row['recipients'], true);
            // print_r($recipients['recipients']);
            foreach($recipients['recipients'] as $recipient){
                if($recipient['phone'] != '' ){
                    $smsCount++;
                }
                if($recipient['email'] != '' ){
                    $emailCount++;
                }
                if(isset($recipient['push'])){
                    if($recipient['push'] != '' ){
                        $pushCount++;
                    }
                }
                    
            }
            $sent = $row['sent'];
            $message = str_replace("\n", "\\n", strip_tags($row['message']));
            $json[] = array("created" => $sent,"message" => $message,"emails" => $emailCount,"sms" => $smsCount,"push" => $pushCount);
        }
    }
    $jsonResult = "{\"alerts\": ".json_encode($json)."}";
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}

if(isset($_POST['orgUuid'])){
    $message = $_POST['message'];
    $expiration = $_POST['message'];
    $methods = explode(',', $_POST['methods']);

    // mailgun client setup
    // $mgClient = Mailgun::create('key-b097370bf2735145ecd6105b49b607d7', 'https://api.mailgun.net/v3');
    // $mgClient = Mailgun::create('key-b097370bf2735145ecd6105b49b607d7', 'https://api.mailgun.net/v3/sandboxb91a3cc714d74339a895e3349adb7f4b.mailgun.org');
    $domain = "mg.safekeepingapp.com";
    $counter = 1;
    $emailCounter = 0;
    $emails = array();
    $uniqueEmails = array();
    $recipientVars = "{";
    $smsCounter = 0;
    $batch = 0;
            
    // twilio client setup
    $auth_token = '8be15ef49cea972db8618cf596aa7b45';
    $account_sid = 'AC27651d18c4f5a2c5c8ef98a4929f4c0f';
    $twilio_number = "+13173448895";
    $messageSid = 'MG243d43d2ab0812ead58a2d7650d6e3f0';
    $serviceSid = 'IS44b5855919c271d760c236f3f83d3b72';
    $client = new Client($account_sid, $auth_token);

    // var_dump($client);

    // find and replace common UCS2 values and convert to GSM encodables
    $s =  str_replace("\t", " ", $message);
    $s =  str_replace("\n", " ", $s);
    $s =  str_replace("•", "*", $s);
    $s =  str_replace("“", '"', $s);
    $s =  str_replace("”", '"', $s);
    $s =  str_replace("‘", "'", $s);
    $s =  str_replace("’", "'", $s);
    $message = $s;
    $sms_text = $message;
    $email_text = nl2br($message);
    
    $bindings = array();
    $uniqueBindings = array();
    $phones = array();
    $recipients = array();
    // get recipients
    if($facId != ''){
        $query = "SELECT p.firstName, p.lastName, u.phone, u.lastname, u.firstname, u.id, u.contacttype
        FROM relations r
        LEFT JOIN user u 
        ON r.uid = u.uid
        LEFT JOIN patients p 
        ON p.patientId = r.patient
        WHERE r.orgUuid = '$orgUuid'
        AND r.facId = '$facId'
        -- AND u.active = 'true' 
        AND p.patientStatus != 'Discharged'
        AND u.type = 'primary'
        ORDER BY p.lastName, p.firstName, u.lastname, u.firstname";
    } else {
        // var_dump($orgUuid);
        $query = "SELECT p.firstName, p.lastName, u.phone, u.lastname, u.firstname, u.id, u.contacttype
        FROM relations r
        LEFT JOIN user u 
        ON r.uid = u.uid
        LEFT JOIN patients p 
        ON p.patientId = r.patient
        WHERE r.orgUuid = '$orgUuid'
        AND p.patientStatus != 'Discharged'
        -- AND u.active = 'true' 
        -- AND u.type = 'primary'
        ORDER BY p.lastName, p.firstName, u.lastname, u.firstname";
    }
    
    // echo $query; 
    
    $result = mysqli_query($link, $query) or die (mysqli_error($link) . " : death while finding message recipients");
    

    $authRow = mysqli_fetch_assoc($result);
    // var_dump($authRow);
    // iterate over results
    if(mysqli_num_rows($result) > 0){
        // var_dump(mysqli_fetch_assoc($result));
        while($row = mysqli_fetch_assoc($result)){
            // $recipients = "{\"recipients\": [";
            
            // $recipients['recipients'] = (Array) [];

            if($row['id'] != "" && ($row['contacttype'] == "email" || $row['contacttype'] == "both" || $row['contacttype'] == "text" || $row['contacttype'] = NULL ) ){
                $id = $row['id'];
                if(!in_array($id, $emails)){
                    $id = $row['id'];
                    $email = $row['id'];
                    $emailCounter++;
                    $emails[] = $email;
                    $recipientVars .= "\"$email\": {\"count\": $counter, \"id\":\"$id\"},";
                }
            }

            if($row['phone'] != "" && ($row['contacttype'] == "sms" || $row['contacttype'] == "both"|| $row['contacttype'] == "text" || $row['contacttype'] == NULL) ){
                $phone = $row['phone'];
                if(!in_array($phone, $phones)){
                    // var_dump("entro");
                    $phones[] = $phone;
                    // var_dump(["phones" => $phones]);
                    $smsCounter++;
                    // create a twilio binding to SMS for this user
                    $bindings[] = "{\"binding_type\": \"sms\", \"address\": \"+1$phone\"}";
                    
                    // $recipients .= '{"patientFirstName":"'.$row['firstName'].'","patientLastName":"'.$row['lastName'].'",';
                    // $recipients .= '"userFirstName":"'.$row['firstname'].'","userLastName":"'.$row['lastname'].'",';
                    // $recipients .= '"phone":"'.$row['phone'].'","email":"'.$row['id'].'"},';
                    
                    $recipients[] = array("patientFirstName" => $row['firstName'],  "patientLastName" =>  $row['lastName'], "userFirstName" => $row['firstname'], "userLastName" => $row['lastname'], "phone" => $row['phone'], "email" => $row['id']);
                    // var_dump(["info recipients" =>  array("patientFirstName" => $row['firstName'],  "patientLastName" =>  $row['lastName'], "userFirstName" => $row['firstname'], "userLastName" => $row['lastname'], "phone" => $row['phone'], "email" => $row['id'])]);
                    // array_push($recipients['recipients'], array("patientFirstName" => $row['firstName'],  "patientLastName" =>  $row['lastName'], "userFirstName" => $row['firstname'], "userLastName" => $row['lastname'], "phone" => $row['phone'], "email" => $row['id']));
                }
                    
            }
            $counter++;
        } 

        // var_dump(["bindings" => $bindings, "recipientVars" => $recipientVars, "recipients" => $recipients]);
        
        // finished collecting user lists
        if( in_array('sms', $methods) || in_array('preferred', $methods)){ 
            $client->notify->services($serviceSid)->notifications->create([
                "toBinding" => $bindings,
                "body" => "$sms_text"]);
        }
        if( in_array('email', $methods) || in_array('preferred', $methods)){ 
            $recipientVars = substr($recipientVars, 0, -1)."}";
            $params =  array(
                'from'    => 'Safekeeping Alerts <no.reply@mg.safekeepingapp.com>',
                'to'      => $emails,
                'subject' => $email_subject,
                'text'    => $sms_text,
                'html'    => $email_text,
                'recipient-variables' => $recipientVars
            );
            $result = $mgClient->messages()->send($domain, $params);
        }

        $results = "{\"recipients\": ".json_encode($recipients)."}";
    
        $sender = "admin@safekeepingapp.com";
        $query = "INSERT INTO facilityAlerts(orgUuid, facility, sender, sent, message, recipients) 
                    VALUES ('$orgUuid', '$facId', '$sender', NOW(), '$sms_text', '$results')";
        $result = mysqli_query($link, $query) or die (mysqli_error($link) . " : death while saving message");
        echo $results;
    }
}
