<?php
require 'checkAuth.php';
require 'db_connect.php';


// sanitize vars
$orgUuid = trim(mysqli_real_escape_string($link, $_GET['orgUuid']));

// find authorization for this user and patient
$authQuery = "SELECT * FROM user WHERE uid = '$skuser' AND (type = 'orgAdmin' OR type = 'superAdmin')";
$authResult = mysqli_query($link, $authQuery);
if(mysqli_num_rows($authResult) !=  1) {
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized\"}");
}

// authenticated token, authorized for this patient so send json
$query = "SELECT f.facilityName, d.facId, f.bedCount
FROM facilities f 
WHERE f.orgUuid = '$orgUuid'";
$result = mysqli_query($link, $query);

$json = '{"facilities":[';
while($row = mysqli_fetch_assoc($result)){
    $json .= '{"facilityName":"'.$row['facilityName'].'","facId":'.$row['facId'].',"bedCount":'.$row['bedCount'].',"settings":'.$row['json'].'},';
    }
    $json = substr($json, 0 , -1)."]}";
http_response_code(200);
header('Content-Type: application/json');
exit($json);