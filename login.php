<?php
require 'checkAuth.php';
require 'db_connect.php';
// PCC user
// $skuser = '1504956211.92415';
$subject = 'logins';
// since token is verified, update values in database
require './apiServer/vendor/autoload.php';
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

if($skuser == '~orgadmin' || $skuser == '~facadmin' ){
    $userType = 2;
    // instantiate database and facilities object
    // $database = new Database();
    // $db = $database->getConnection();

    // $stmt = $db->prepare("SELECT uid, firstname, lastname, type, orgUuid FROM user WHERE uid=?");
    // $stmt->bindParam("s", $skuser);
    // $stmt->execute();

    // $num = $stmt->rowCount();
    
    // check if more than 0 record found
    // if($num>0){
            
    //     http_response_code(200);
    //     $row = json_encode($row);
    //     header('Content-Type: application/json');
    //     exit($row);
    // }else{
    //     header('Content-Type: application/json');
    //     exit('{"error":"No account registered for this Id (1)"}');
    // }
    $query = "SELECT uid, firstname, lastname, type, orgUuid FROM user WHERE uid = '$skuser'";

    $result = mysqli_query($link, $query);
    if(mysqli_num_rows($result) == 1){
        $row = mysqli_fetch_assoc($result);
        // create user info json
        http_response_code(200);
        $row = json_encode($row);
        header('Content-Type: application/json');
        exit($row);
    } else {
        header('Content-Type: application/json');
        exit('{"error":"No account registered for this Id (1)"}');
    }
}    
$serviceAccount = ServiceAccount::fromJsonFile('safekeepingv2-firebase-adminsdk-534k4-e5b676e51b.json');

$auth = (new Factory)
        ->withServiceAccount($serviceAccount)
        ->createAuth();
    try {
        $verifiedIdToken = $auth->verifyIdToken($token);
    } catch (\InvalidArgumentException $e) {
        http_response_code(401);
        $message = $e->getMessage();
        exit("{\"error\":\"authorization error: invalidArgumentException\"}");
    } catch (InvalidToken $e) { 
        http_response_code(401);
        exit("{\"error\":\"authorization error: invalidToken\"}");
    }

    $uid = $verifiedIdToken->getClaim('sub');
    $user = $auth->getUser($uid);
    $json = json_encode($user);
    file_put_contents('traffic.log', "$json \n\n", FILE_APPEND | LOCK_EX);        


// collect and sanitize firebase claims
$uid = mysqli_real_escape_string($link, $verifiedIdToken->getClaim('sub'));
$alg = '';
$exp = mysqli_real_escape_string($link, $verifiedIdToken->getClaim('exp'));
$iat = mysqli_real_escape_string($link, $verifiedIdToken->getClaim('iat'));
$aud = mysqli_real_escape_string($link, $verifiedIdToken->getClaim('aud'));
$iss = mysqli_real_escape_string($link, $verifiedIdToken->getClaim('iss'));
$sub = mysqli_real_escape_string($link, $verifiedIdToken->getClaim('sub'));
$auth_time = mysqli_real_escape_string($link, $verifiedIdToken->getClaim('auth_time'));

// check to see if user exists, then update or insert
$query = "SELECT * FROM firebaseTokens WHERE uid = '$uid'";
$result = mysqli_query($link, $query);
if(mysqli_num_rows($result) == 1){
    // update table
    $currentToken = mysqli_fetch_assoc($result);
    $updateQuery = "UPDATE firebaseTokens 
        SET login = NOW(),
            logout = NULL,
            uid = '$uid', 
            alg = '$alg', 
            exp = '$exp', 
            iat = '$iat', 
            aud = '$aud', 
            iss = '$iss', 
            sub = '$sub',
            auth_time = '$auth_time',
            jwt = '$token',
            userType = '$userType'
        WHERE uid = '$uid'";
        $updateResult = mysqli_query($link, $updateQuery);
    $logQuery = "INSERT INTO $subject (id, time, success) VALUES ('$uid', NOW(), 1)";
    $logResult = mysqli_query($link, $logQuery) or die (mysqli_error($link) . " : death while logging request");

} else if(mysqli_num_rows($result) == 0){
    // insert into table
    $insertQuery = "INSERT INTO firebaseTokens (login, uid, alg, exp, iat, aud, iss, sub, auth_time, jwt, userType)
        VALUES (NOW(), '$uid', '$alg', '$exp', '$iat', '$aud', '$iss', '$sub', '$auth_time', '$token', '2')";
file_put_contents('traffic.log',  "$insertQuery\n\n", FILE_APPEND | LOCK_EX);        
    $result = mysqli_query($link, $insertQuery);
    $logQuery = "INSERT INTO $subject (id, time, success) VALUES ('$uid', NOW(), 1)";
    $logResult = mysqli_query($link, $logQuery) or die (mysqli_error($link) . " : death while logging request");

} else {
    // too many firebase tokens for this user, error out 
    $logQuery = "INSERT INTO $subject (id, time, success) VALUES ('$uid', NOW(), 0)";
    $logResult = mysqli_query($link, $logQuery) or die (mysqli_error($link) . " : death while logging request");
    http_response_code(401);
    header('Content-Type: application/json');
    exit("{\"error\":\"authorization error.\"}");
}

// token was accepted and updated in table, now check for PCC users and update

// check customAttributes for user type, orgUuid, access_token, provider
// if userType = pcc, log the pcc token info
if($userType == '1'){
    $orgUuid = $user->customAttributes['orgUuid'];
    $userType = $user->customAttributes['userType'];
    $access_token = $user->customAttributes['access_token'];
    $provider = $user->customAttributes['provider'];

    
    /*//  update PCC permissions
    include 'getPccUserInfo.php';
    $pccPermisions = getPccUserInfo($orgUuid, $access_token);
    // replace permssions with new permissions from PCC
    $query = "SELECT * FROM pccTokens WHERE uid = '$uid'";
    $result = mysqli_query($link, $query);
    if(mysqli_num_rows($result) == 1){
        $query = "UPDATE pccTokens 
            SET uid = '$uid', 
            time = NOW(), type = '$userType', orgUuid = '$orgUuid', 
            access_token = '$access_token', json = '$pccPermissions'
            WHERE uid = '$uid'";
    }*/
    // get user info
    
} 
// get user info

$query = "SELECT id, uid, firstname, lastname, type FROM user WHERE uid = '$uid'";

$result = mysqli_query($link, $query);
if(mysqli_num_rows($result) == 1){
    $row = mysqli_fetch_assoc($result);
    // $avatar = "/file.php?id="$row['avatar'];
    // $row['avatar'] = $avatar;
    // create user info json
    http_response_code(200);
    $row = json_encode($row);
    header('Content-Type: application/json');
    exit($row);
} else {
    header('Content-Type: application/json');
    exit('{"error":"No account registered for this Id"}');
}
?>
