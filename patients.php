<?php
require 'checkAuth.php';
require 'db_connect.php';

$orgUuid = trim(mysqli_real_escape_string($link, $_GET['orgUuid']));
$facId = trim(mysqli_real_escape_string($link, $_GET['facId']));

// find authorization for this user
$authQuery = "SELECT * FROM user WHERE uid = '$skuser'";
$authResult = mysqli_query($link, $authQuery);
$authRow = mysqli_fetch_assoc($authResult);
if(mysqli_num_rows($authResult) !=  1) {
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized\"}");
}

if($authRow['orgUuid'] != $orgUuid){
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized (org)\"}");
}
if($authRow['type'] == 'orgAdmin' && $authRow['orgUuid'] != $orgUuid){
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized (user)\"}");
}
if($authRow['type'] == 'admin' && $authRow['orgUuid'] != $orgUuid){
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized (user)\"}");
}
// authenticated
if($authRow['type'] == 'superAdmin'){
    if(strlen($facId) > 0){
        $query = "SELECT json FROM patients WHERE orgUuid = '$orgUuid' AND facId= '$facId' AND patientStatus != 'Discharged'";
    } else {
        $query = "SELECT json FROM patients WHERE orgUuid = '$orgUuid' AND patientStatus != 'Discharged'";
    }
    $result = mysqli_query($link, $query);
    $json = [];
    while($row = mysqli_fetch_assoc($result)){
        $json[] = $row['json'];
    }
    $jsonResult = "{\"patients\": ".json_encode($json)."}";
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
if($authRow['type'] == 'orgAdmin'){
    if(strlen($facId) > 0){
        $query = "SELECT json FROM patients WHERE orgUuid = '$orgUuid' AND facId= '$facId' AND patientStatus != 'Discharged'";
    } else {
        $query = "SELECT json FROM patients WHERE orgUuid = '$orgUuid' AND patientStatus != 'Discharged'";
    }
    $result = mysqli_query($link, $query);
    $json = [];
    while($row = mysqli_fetch_assoc($result)){
        $json[] = $row['json'];
    }
    $jsonResult = "{\"patients\": ".json_encode($json)."}";
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
if($authRow['type'] == 'admin'){
    $query = "SELECT json FROM patients WHERE orgUuid = '$orgUuid' AND facId= '$facId' AND patientStatus != 'Discharged'";
    $result = mysqli_query($link, $query);
    $json = [];
    while($row = mysqli_fetch_assoc($result)){
        $json[] = $row['json'];
    }
    $jsonResult = "{\"patients\": ".json_encode($json)."}";
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
else {
    http_response_code(200);
    header('Content-Type: application/json');
    exit("{\"data\": []}");
}