<?php
require 'checkAuth.php';
// require 'db_connect.php';
require 'db_connect.php';
// PCC user
// $skuser = '1504956211.92415';

// find authorization for this user
 
$authQuery = "SELECT type, orgUuid, facilities FROM user WHERE uid = '$skuser' AND type != 'primary'";
$authResult = mysqli_query($link, $authQuery);
if(mysqli_num_rows($authResult) !=  1) {
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized\"}");
}

// authenticated
$authRow = mysqli_fetch_assoc($authResult);
if($authRow['type'] == 'orgAdmin'){
    $orgUuid = $authRow['orgUuid'];
    $query = "SELECT orgUuid, orgName, orgShortName, facilityId, facilityName FROM facilities WHERE orgUuid='$orgUuid' ORDER BY orgName, facilityName ASC";
    
    $result = mysqli_query($link, $query);
    $json = [];
    while($row = mysqli_fetch_assoc($result)){
        $json[] = $row;
    }
    $jsonResult = json_encode(array("facilities" => $json));
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
if($authRow['type'] == 'superAdmin'){
    $query = "SELECT orgUuid, orgName, orgShortName, facilityId, facilityName FROM facilities ORDER BY orgName, facilityName ASC";
    $result = mysqli_query($link, $query);
    $json = [];
    while($row = mysqli_fetch_assoc($result)){
        $json[] = $row;
    }
    $jsonResult = json_encode(array("facilities" => $json));

    // "{\"facilities\": ]}" }
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
if($authRow['type'] == 'admin'){
    $orgUuid = $authRow['orgUuid'];
    
    $data = json_decode($authRow['facilities'], true);
    $json = [];
    foreach($data['facilities'] as $fac){
        $query = "SELECT orgUuid, orgName, orgShortName, facilityId, facilityName FROM facilities WHERE orgUuid = '$orgUuid' AND facilityId = '".$fac['facId']."' ";
        $result = mysqli_query($link, $query);
        $row = mysqli_fetch_assoc($result);
        $json[] = $row;
    }
    $jsonResult = json_encode(array("facilities" => $json));
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
else {
    http_response_code(200);
    header('Content-Type: application/json');
    exit("{\"data\": []}");
}