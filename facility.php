<?php
require 'checkAuth.php';
require 'db_connect.php';

if(isset($_GET['orgUuid'])){
    $orgUuid = trim(mysqli_real_escape_string($link, $_GET['orgUuid']));
    $facId = trim(mysqli_real_escape_string($link, $_GET['facId']));
} else {
    $orgUuid = trim(mysqli_real_escape_string($link, $_POST['orgUuid']));
    $facId = trim(mysqli_real_escape_string($link, $_POST['facId']));
}
// find authorization for this user and patient
    $authQuery = "SELECT * FROM user WHERE uid = '$skuser'";
    $authResult = mysqli_query($link, $authQuery);
    $authRow = mysqli_fetch_assoc($authResult);

    if($authRow['type'] == 'admin' && $authRow['orgUuid'] != $orgUuid){
        http_response_code(403);
        header('Content-Type: application/json');
        header('Accept: application/json');
        exit("{\"error\": \"unauthorized (admin)\"}");
    }
    if($authRow['type'] == 'orgAdmin' && $authRow['orgUuid'] != $orgUuid){
        http_response_code(403);
        header('Content-Type: application/json');
        header('Accept: application/json');
        exit("{\"error\": \"unauthorized (org)\"}");
    }
if(isset($_GET['orgUuid'])){
    // authenticated token, authorized for this patient so send json
    $query = "SELECT f.facilityName, f.facilityId, f.bedCount, f.dataTypes 
    FROM facilities f 
    WHERE f.orgUuid = '$orgUuid'
    AND f.facilityId = '$facId'";
    $result = mysqli_query($link, $query);
    if(mysqli_num_rows($result) > 0){
        $json = [];
        while($row = mysqli_fetch_assoc($result)){
            $json[] = array("facilityName" => $row['facilityName'],"facId" => $row['facilityId'],"bedCount" => $row['bedCount'],"settings" => $row['dataTypes']);
        }
        $json = "{\"facility\": ".json_encode($json)."}";
        http_response_code(200);
        header('Content-Type: application/json');
        header('Accept: application/json');
        exit($json);
    } else {
        // no items found
        http_response_code(200);
        header('Content-Type: application/json');
        header('Accept: application/json');
        exit("{\"data\": []}");
    }
} 

if(isset($_POST['orgUuid'])){
  $orgUuid = trim(mysqli_real_escape_string($link, $_POST['orgUuid']));
  $facId = trim(mysqli_real_escape_string($link, $_POST['facId']));
  $pair = trim($_POST['pair']);
  $thing = trim($_POST['value']);
  // find authorization for this user and patient
  $authQuery = "SELECT * FROM user WHERE uid = '$skuser'";
  $authResult = mysqli_query($link, $authQuery);
  $authRow = mysqli_fetch_assoc($authResult);
  if($authRow['type'] == 'superAdmin' || ($authRow['type'] == 'orgAdmin' && $authRow['orgUuid'] == $orgUuid)) {
      $fQuery = "SELECT dataTypes FROM facilities WHERE orgUuid = '$orgUuid' AND facilityId = '$facId'";
      $fResult = mysqli_query($link, $fQuery);
      $fRow = mysqli_fetch_assoc($fResult);
      $dataTypes = json_decode($fRow['dataTypes'], true);
    function findandReplace(&$array,$pair,$thing) {
        foreach($array as $key => &$value)
        {
            if(is_array($value))
            {
                findandReplace($value,$pair,$thing);
            }
            else{
                if ($key == $pair) {
                    $array[$pair] = $thing;
                }
            }
        }
        return $array;
    }
    
      $newData = json_encode(findandReplace($dataTypes,$pair,$thing));
      $uQuery = "UPDATE facilities SET dataTypes = '$newData' WHERE orgUuid = '$orgUuid' AND facilityId = '$facId'";
      $uResult = mysqli_query($link, $uQuery);
      http_response_code(200);
      exit("$newData");        
  }
  else {
      http_response_code(403);
      header('Content-Type: application/json');
      exit("{\"error\": \"unauthorized change request: bad user type or wrong organization\"}");
  }
}