<?php
require 'checkAuth.php';
require 'db_connect.php';

// find authorization for this user
$authQuery = "SELECT * FROM user WHERE uid = '$skuser'";
$authResult = mysqli_query($link, $authQuery);
if(mysqli_num_rows($authResult) !=  1) {
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized\"}");
}
$orgUuid = trim(mysqli_real_escape_string($link, $_GET['orgUuid']));
$facId = "";
if(isset($_GET['facId'])){
    $facId = trim(mysqli_real_escape_string($link, $_GET['facId']));
}

// authenticated
$authRow = mysqli_fetch_assoc($authResult);
if($authRow['type'] == 'superAdmin'){
    if(strlen($facId) > 0){
        $query = "SELECT json FROM metrics WHERE orgUuid = '$orgUuid' AND facId= '$facId'";
    } else {
        $query = "SELECT json FROM metrics WHERE orgUuid = '$orgUuid'";
    }
    $result = mysqli_query($link, $query);
    $json = [];
    while($row = mysqli_fetch_assoc($result)){
        $json[] = $row['json'];
    }
    $jsonResult = "{\"metrics\": ".json_encode($json)."}";

    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
if($authRow['type'] == 'orgAdmin'){
    if(strlen($facId) > 0){
        $query = "SELECT json FROM metrics WHERE orgUuid = '$orgUuid' AND facId= '$facId'";
    } else {
        $query = "SELECT json FROM metrics WHERE orgUuid = '$orgUuid'";
    }
    $result = mysqli_query($link, $query);
    $json = [];
    while($row = mysqli_fetch_assoc($result)){
        $json[] = $row['json'];
    }
    $jsonResult = "{\"metrics\": ".json_encode($json)."}";
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
if($authRow['type'] == 'admin'){
    $query = "SELECT json FROM metrics WHERE orgUuid = '$orgUuid' AND facId= '$facId'";
    $result = mysqli_query($link, $query);
    $json = [];
    while($row = mysqli_fetch_assoc($result)){
        $json[] = $row['json'];
    }
    $jsonResult = "{\"metrics\": ".json_encode($json)."}";
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
else {
    http_response_code(200);
    header('Content-Type: application/json');
    exit("{\"data\": []}");
}