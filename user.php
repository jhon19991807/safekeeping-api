<?php
require 'checkAuth.php';
require 'db_connect.php';

$uid = trim(mysqli_real_escape_string($link, $_POST['uid']));
$orgUuid = trim(mysqli_real_escape_string($link, $_POST['orgUuid']));
$facId = trim(mysqli_real_escape_string($link, $_POST['facId']));
$lastName = trim(mysqli_real_escape_string($link, $_POST['lastName']));
$firstName = trim(mysqli_real_escape_string($link, $_POST['firstName']));
$method = trim(mysqli_real_escape_string($link, $_POST['method']));
$active = trim(mysqli_real_escape_string($link, $_POST['active']));
$email = trim(mysqli_real_escape_string($link, $_POST['email']));
$phone  = trim(mysqli_real_escape_string($link, $_POST['phone']));

// find authorization for this user and patient
$authQuery = "SELECT * FROM user WHERE uid = '$skuser'";
$authResult = mysqli_query($link, $authQuery);
$authRow = mysqli_fetch_assoc($authResult);
/*
if($authRow['type'] != 'admin' && $authRow['type'] != 'orgAdmin'  && $authRow['type'] != 'superAdmin'){
        http_response_code(403);
        header('Content-Type: application/json');
        exit("{\"error\": \"unauthorized user type\"}");
}

if($authRow['orgUuid'] != $orgUuid){
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized for this organization\"}");
}
*/
if($uid == ''){
    http_response_code(411);
    header('Content-Type: application/json');
    exit("{\"error\": \"required values not set\"}");
}


// passed validation checks, update user
$query = "UPDATE user SET  
lastname = '$lastName',
firstname = '$firstName',
contacttype = '$method',
active = '$active',
id = '$email',
phone = '$phone'
WHERE uid = '$uid'";
$result = mysqli_query($link, $query);
$query = "SELECT p.firstName as patientFirst, p.lastName as patientLast, 
    u.uid, u.phone, u.lastname, u.firstname, u.active, u.type, u.id, u.currentlogin, u.logincount, u.contacttype,
    r.facId, r.orgUuid, p.patientId, p.patientStatus, p.admissionDate, p.dischargeDate 
    FROM relations r
    JOIN user u 
    ON r.id = u.id
    JOIN patients p 
    ON p.patientId = r.patient
    WHERE r.orgUuid = '$orgUuid'
    AND r.facId = '$facId'
    AND u.uid = '$uid'
    AND p.patientStatus != 'Discharged'
    ORDER BY p.lastName, p.firstName, u.lastname, u.firstname";
$result = mysqli_query($link,$query);
$row = mysqli_fetch_assoc($result);
http_response_code(200);
exit(json_encode($row));
?>