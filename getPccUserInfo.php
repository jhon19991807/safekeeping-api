<?php 
function getPccUserInfo($orgId,$authToken){
        $url = "https://connect.pointclickcare.com/api/public/preview1/orgs/$orgUuid/userinfo";
        $headers = array(
            "Cache-Control: no-cache",
            "Pragma: no-cache",
            "Authorization: Bearer " . $authToken
            );
    
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 60);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        $data = curl_exec($ch);
        $pccuserinfo = 0;
        if (curl_errno($ch)) {
            return (print "Error: " . curl_error($ch));
        } else {   
            echo($data);
        }
        curl_close($ch);
    }
?>