<?php
require 'checkAuth.php';
require 'db_connect.php';
// include '../actions/getpatients.php';
// include '../actions/updatebasicpatient.php';
// require '../twilio-php-master/Twilio/autoload.php';
use Twilio\Rest\Client;


$email = mysqli_real_escape_string($link,trim(strtolower($_POST['email'])));
$firstName = mysqli_real_escape_string($link,trim($_POST['firstName']));
$lastName = mysqli_real_escape_string($link,trim($_POST['lastName']));
$pid = "";
if(isset($_POST['patientId'])){
	$pid = mysqli_real_escape_string($link,trim($_POST['patientId']));
}
$contactType = mysqli_real_escape_string($link,trim($_POST['contactType']));
$phone = mysqli_real_escape_string($link,trim(preg_replace('~\D~', '',$_POST['phone'])));

$orgUuid = mysqli_real_escape_string($link,trim($_POST['orgUuid']));
$facId = "";

if(isset($_POST['facId'])){
	$facId = mysqli_real_escape_string($link,trim($_POST['facId']));
}

$pid = strtolower($pid);
if($pid == 'list' || $orgUuid == '3270235B-CD46-4C9B-84AC-25FDD868AF78' ){
	if(strlen($email) > '3'){
		$id = $email;
		$uid = "~".$email;
	} else {
		$id = $phone;
		$uid = "~".$phone;
	}
	// create relations entry
	$query = "INSERT INTO relations (uid, id, patient, orgid, orgUuid, facId, created) 
		VALUES ('$uid','$id', '$pid', '$orgId', '$orgUuid', '$facId', NOW() );";
		$result = mysqli_query($link, $query) or die (mysqli_error($link) . " : death while creating relation");
	// create user entry
	$hash = password_hash(time(), PASSWORD_BCRYPT);
	$type = 'primary';
	// check to see if user exists!!
	$query = "SELECT * FROM user WHERE id = '$email' OR id = '$phone'";
	$result = mysqli_query($link, $query) or die (mysqli_error($link) . " : death while searching for existing user");
	if(mysqli_num_rows($result) != 0){
		// update existing user
		$query = "UPDATE user SET uid = '$uid', organization = '$organization', orgUuid = '$orgUuid'
			WHERE id = '$email' OR id = '$phone'";
		$result = mysqli_query($link, $query) or die (mysqli_error($link) . " : death while adding user");
	} else {
		$query = "INSERT INTO user (uid, id, password, firstname, lastname, type, organization, orgid, orgUuid, phone, contacttype) 
    	VALUES ('$uid','$id', '$hash', '$firstName', '$firstName', '$type', '$organization', '$orgId', '$orgUuid', '$phone', '$contacttype');";
		$result = mysqli_query($link, $query) or die (mysqli_error($link) . " : death while adding user");
	}
echo 'added to broadcast list';
exit;
}
// get token, then get patients
$query = "SELECT * FROM authtoken WHERE orgUuid = '$orgUuid';";
$result = mysqli_query($link, $query) or die (mysqli_error($link) . " : death while finding token");
$row = mysqli_fetch_assoc($result);
$newToken = $row['token'];
$certificate = $row['certificate'];
$orgId = $row['orgId'];


// get Organization Info
$query = "SELECT * FROM facilities WHERE orgUuid = '$orgUuid'";
$result = mysqli_query($link,$query);
$row = mysqli_fetch_assoc($result);
$facility = $row['facilityId'];
$organization = $row['orgName'];
$facilityName = $row['facilityName'];

//get actual patient ID from external Id
$pfirst = "";
$patient = "";
if(strlen($pid)> 0){
	$pquery = "SELECT * from patients WHERE patientExternalId = '$pid' AND facId = '$facId' AND orgUuid = '$orgUuid';";
	$presult = mysqli_query($link, $pquery) or die (mysqli_error($link) . " : $pquery");
	$prow = mysqli_fetch_assoc($presult);
	
	$pfirst = $prow['firstName'];
	$patient = $prow['patientId'];
}

// create regcode  

function generateRandomString($length = 10) {
    $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
    $charactersLength = strlen($characters);
    $randomString = '';
    for ($i = 0; $i < $length; $i++) {
        $randomString .= $characters[rand(0, $charactersLength - 1)];
    }
    return $randomString;
}

$myRandomString = generateRandomString();

$regcode = urlencode(base64_encode("$pfirst*$firstName*$myRandomString"));

// create new invitation entry 
$query = "INSERT INTO 
	invitations (id, type, firstname, lastname, pid, organization, orgid, facility, invitation, waiver, regcode, created, phone, contactType, orgUuid) 
	VALUES ( 
		'$email', 'primary', '$firstName', '$firstName', '$pid', '$organization','$orgId','$facId', 'Pending Verification','', '$regcode', NOW(), '$phone','$contactType', '$orgUuid');";
$result = mysqli_query($link, $query) or die (mysqli_error($link) . " : $query");

if($contactType == 'sms' || $contactType == 'both'){
	
	$account_sid = 'AC27651d18c4f5a2c5c8ef98a4929f4c0f';
	$auth_token = '8be15ef49cea972db8618cf596aa7b45';
	$twilio_number = "+13173448895";
	$client = new Client($account_sid, $auth_token);
	$client->messages->create(
		"+1$phone",
		array(
			'from' => $twilio_number,
			'body' =>  "$facilityName has invited you to enroll in a free resident engagement tool, Safekeeping. This tool allows access to portions your loved ones medical information and facility alerts even when you are unable to visit. Please go to https://app.safekeepingapp.com/register.php?id=$regcode to set up your account. If you have any questions please contact the facility admissions department."
		)
	);
	$now = microtime(TRUE);
	$query = "INSERT INTO communication(sent, type, target, orgid, orgUuid, facility, patient, medium) VALUES(NOW(), 'invitation', '$phone', '$orgId', '$orgUuid', '$facility', '$patient', '$contactType');";
	mysqli_query($link, $query) or die (mysqli_error($link) . " : death while looking up invitation");
}
if($contactType == 'email' || $contactType == 'both'){
	// send email

	$body ="<html style='font: 16px sans-serif;'>
	<body>
			<div style='background: #81CEC8; color: #fff; padding: 15px;font-size: 20px; font-weight: 700;'>
		$firstName- </div>
			<br />
		$facilityName has invited you to enroll in a free resident engagement tool, Safekeeping. This tool allows access to portions your loved ones medical 
		information and facility alerts even when you are unable to visit. Please go to 
		<a href=\"https://app.safekeepingapp.com/register.php?id=$regcode\">https://app.safekeepingapp.com/register.php?id=$regcode</a> to set up your account. If you have any 
		questions please contact the facility admissions department.
	</body>		
	</html>";
	$text="Invitation to access SafeKeeping\n
		\n
	$firstName- \n
	$facilityName has invited you to enroll in a free resident engagement tool, Safekeeping. This tool allows access to portions your loved ones medical 
	information and facility alerts even when you are unable to visit. Please go to 
	<a href=\"https://app.safekeepingapp.com/register.php?id=$regcode\">https://app.safekeepingapp.com/register.php?id=$regcode</a> to set up your account. If you have any 
	questions please contact the facility admissions department.";
	
	$ch = curl_init();
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_BASIC);
	curl_setopt($ch, CURLOPT_USERPWD, 'api:key-b097370bf2735145ecd6105b49b607d7');
	curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
	curl_setopt($ch, CURLOPT_URL, 
								'https://api.mailgun.net/v3/mg.safekeepingapp.com/messages');
	curl_setopt($ch, CURLOPT_POSTFIELDS, 
					array('from' => 'Safekeeping Invitations <invitations@mg.safekeepingapp.com>',
							'to' => $email,
							'bcc' => 'Safekeeping Invitations <invitations@safekeepingapp.com>',
							'h:Reply-To' => 'inquiries@safekeepingapp.com',
							'subject' => "$facilityName invites you to join SafeKeeping!",
							'text' => $text,
							'html' => $body));
	$results = curl_exec($ch);
	curl_close($ch);
	$now = microtime(TRUE);
	$query = "INSERT INTO communication(sent, type, target, orgid, orgUuid, facility, patient, medium) VALUES(NOW(), 'invitation', '$email', '$orgId', '$orgUuid', '$facility', '$patient', 'email');";
	mysqli_query($link, $query) or die (mysqli_error($link) . " : death while looking up invitation");
}
http_response_code(200);
header('Content-Type: application/json');
exit("{\"success\": \"invitation sent\"}");
?>