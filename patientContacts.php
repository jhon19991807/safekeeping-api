<?php
require 'checkAuth.php';
require 'db_connect.php';

// PCC user
// $skuser = '1504956211.92415';

$subject = 'patientContacts';

// sanitize vars
$orgUuid = trim(mysqli_real_escape_string($link, $_GET['orgUuid']));
$patientId = trim(mysqli_real_escape_string($link, $_GET['patientId']));

// find authorization for this user and patient
$patientQuery = "SELECT * FROM relations WHERE uid = '$skuser' AND orgUuid = '$orgUuid' and patient = '$patientId'";
$result = mysqli_query($link, $patientQuery);
if(mysqli_num_rows($result) ===  1){
    // authenticated token, authorized for this patient so send json
    $query = "SELECT json FROM $subject WHERE orgUuid = '$orgUuid' AND patientId = '$patientId'";
    $result = mysqli_query($link, $query);
    if(mysqli_num_rows($result) ===  1){
        $row = mysqli_fetch_assoc($result);
        $json = $row['json'];
        // log result to table
        $logQuery = "INSERT INTO ".$subject."Log (id, orgUuid, patientId, time, json) VALUES ('$skuser', '$orgUuid', '$patientId', NOW(), '$json')";
        $logResult = mysqli_query($link, $logQuery);
        http_response_code(200);
        header('Content-Type: application/json');
        exit($json);
    } else {
        // no items found
        $logQuery = "INSERT INTO ".$subject."Log (id, orgUuid, patientId, time) VALUES ('$skuser', '$orgUuid', '$patientId', NOW())";
        $logResult = mysqli_query($link, $logQuery) or die (mysqli_error($link) . " : death while logging request");
        http_response_code(200);
        header('Content-Type: application/json');
        exit("{\"data\": []}");
    }
}
else {
    // no items found
    $logQuery = "INSERT INTO ".$subject."Log (id, orgUuid, patientId, time) VALUES ('$skuser', '$orgUuid', '$patientId', NOW())";
    $logResult = mysqli_query($link, $logQuery) or die (mysqli_error($link) . " : death while logging request");
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized for this patient\"}");
}