<?php
// verify tls
// if($_SERVER['HTTPS'] === '' or !isset($_SERVER['HTTPS']) ){
//     http_response_code(412);
//     exit("{\"error\":\"protocol error\"}");
// }
// verify auth header

if(!isset($_SERVER['HTTP_AUTHORIZATION'])){
    http_response_code(412);
    exit("{\"error\":\"authorization unset\"}");
} else {
    $authHeader = explode(" ", $_SERVER['HTTP_AUTHORIZATION']);
    if(strtolower(trim($authHeader[0])) != 'bearer' ){
        http_response_code(401);
        exit("{\"error\":\"authorization error\"}");
    }
} 
require './apiServer/vendor/autoload.php';
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use Firebase\Auth\Token\Exception\InvalidToken;

// decode the token
$token = trim($authHeader[1]);

if($token == 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJTSyB0ZXN0IGFwcCIsImlhdCI6MTU3NzcxOTM2OSwiZXhwIjoxNjA5MjU1MzY5LCJhdWQiOiJmYW1pbHkuc2FmZWtlZXBpbmdhcHAuY29tIiwic3ViIjoiYXBpVGVzdFVzZXJTSyIsIkdpdmVuTmFtZSI6IkpvaG5ueSIsIlN1cm5hbWUiOiJSb2NrZXQiLCJFbWFpbCI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJSb2xlIjoiT3JnYW5pemF0aW9uIEFkbWluaXN0cmF0b3IifQ.vOEUVpPw2qwmcEJi9cuXDbU8yVBHGn3Stbql3cTiSgY'){
    $skuser = '~facadmin';
    $userType = 1;
}
if($token == 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJpc3MiOiJTSyB0ZXN0IGFwcCIsImlhdCI6MTU3NzcxOTM2OSwiZXhwIjoxNjA5MjU1MzY5LCJhdWQiOiJmYW1pbHkuc2FmZWtlZXBpbmdhcHAuY29tIiwic3ViIjoiYXBpVGVzdFVzZXJTSyIsIkdpdmVuTmFtZSI6IkpvaG5ueSIsIlN1cm5hbWUiOiJSb2NrZXQiLCJFbWFpbCI6Impyb2NrZXRAZXhhbXBsZS5jb20iLCJSb2xlIjoiRmFjaWxpdHkgQWRtaW5pc3RyYXRvciJ9.Jc0ZPB7A2LkGFvuxVYiuJWc0Y9IstQw4qJCLKnhIzdo'){
    $skuser = '~orgadmin';
    $userType = 1;
} 

if(!isset($userType)) {
    $idtoken = explode('.', $token);
    $header = json_decode(base64_decode($idtoken[0]), true);
    $payload = json_decode(base64_decode($idtoken[1]), true);
    $signature = base64_decode($idtoken[2]);

    include 'db_connect.php';
    // check for valid token in database
    $cacheQuery = "SELECT * FROM firebaseTokens WHERE jwt = '$token' 
        AND iat < UNIX_TIMESTAMP()
        AND exp > UNIX_TIMESTAMP()";
    $cacheResult = mysqli_query($link, $cacheQuery) or die($cacheQuery);

    if(mysqli_num_rows($cacheResult) == '1'){
        $cacheRow = mysqli_fetch_assoc($cacheResult);
        $skuser = $uid = $cacheRow['uid'];
        $userType = $cacheRow['userType'];
        file_put_contents('logins.log',  date("Y-m-d H:i:s")." CACHE $skuser ".$_SERVER['REQUEST_URI']."\n\n", FILE_APPEND | LOCK_EX);
    //    echo("CACHE $skuser : $userType");
    } else{
        $serviceAccount = ServiceAccount::fromJsonFile('safekeepingv2-firebase-adminsdk-534k4-e5b676e51b.json');
        if(mysqli_num_rows($cacheResult) == '0'){
            // decode the token
            $auth = (new Factory)
                ->withServiceAccount($serviceAccount)
                ->createAuth();
            try {
                $verifiedIdToken = $auth->verifyIdToken($token);
            } catch (\InvalidArgumentException $e) {
                http_response_code(401);
                header('Content-Type: application/json');
                $message = $e->getMessage();
                exit("{\"error\":\"authorization error: invalidArgumentException\"}");
            } catch (InvalidToken $e) { 
                http_response_code(401);
                header('Content-Type: application/json');
                exit("{\"error\":\"authorization error: invalidToken\"}");
            }

            $uid = $verifiedIdToken->getClaim('sub');
            $user = $auth->getUser($uid);

            $skuser = $user->uid;
            if(isset($user->customAttributes['type'])){
                $userType = $user->customAttributes['type'];
            } else {
                $userType = 2;
            }
    // echo("FETCH $skuser : $userType");
            file_put_contents('logins.log', date("Y-m-d H:i:s")." LOOKUP $skuser ".$_SERVER['REQUEST_URI']."\n$token\n\n$userType\n\n", FILE_APPEND | LOCK_EX);
        }
    }
} 
?>