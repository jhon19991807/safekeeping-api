<?php
require 'checkAuth.php';
require 'db_connect.php';
// $orgUuid = trim(mysqli_real_escape_string($link, $_GET['orgUuid']));
// $facId = trim(mysqli_real_escape_string($link, $_GET['facId']));
$facId = "";
$orgUuid = "";
if(isset($_GET['orgUuid'])){
    $orgUuid = trim(mysqli_real_escape_string($link, $_GET['orgUuid']));
    if(isset($_GET['facId'])){
        $facId = trim(mysqli_real_escape_string($link, $_GET['facId']));
    }
   
} else {
    $orgUuid = trim(mysqli_real_escape_string($link, $_POST['orgUuid']));
    if(isset($_POST['facId'])){
        $facId = trim(mysqli_real_escape_string($link, $_POST['facId']));
    }
}
// find authorization for this user
$authQuery = "SELECT * FROM user WHERE uid = '$skuser' AND (type = 'admin' OR type = 'orgAdmin' OR type = 'superAdmin')";
$authResult = mysqli_query($link, $authQuery);
if(mysqli_num_rows($authResult) !=  1) {
    http_response_code(403);
    header('Content-Type: application/json');
    exit("{\"error\": \"unauthorized\"}");
}
// authenticated
$authRow = mysqli_fetch_assoc($authResult);
if($authRow['type'] == 'superAdmin' || $authRow['type'] == 'orgAdmin' ){
    if(strlen($facId) != ''){
        $query = "SELECT p.firstName as patientFirst, p.lastName as patientLast, 
        u.uid, u.phone, u.lastname, u.firstname, u.active, u.type, u.id, u.uid, u.currentlogin, u.logincount, u.contacttype,
        r.facId, r.orgUuid, p.patientId, p.patientStatus, p.admissionDate, p.dischargeDate 
        FROM relations r
        JOIN user u 
        ON r.uid = u.uid
        JOIN patients p 
        ON p.patientId = r.patient
        WHERE r.orgUuid = '$orgUuid'
        AND r.facId = '$facId'
        -- AND u.active = 'true' 
        -- AND p.patientStatus != 'Discharged'
        GROUP BY r.uid
        ORDER BY p.lastName, p.firstName, u.lastname, u.firstname";
    } else {
        $query = "SELECT p.firstName as patientFirst, p.lastName as patientLast, 
        u.uid, u.phone, u.lastname, u.firstname, u.active, u.type, u.id, u.currentlogin, u.logincount, u.contacttype,
        r.facId, r.orgUuid, p.patientId, p.patientStatus,p.admissionDate, p.dischargeDate 
        FROM relations r
        JOIN user u 
        ON r.uid = u.uid
        JOIN patients p 
        ON p.patientId = r.patient
        WHERE r.orgUuid = '$orgUuid'
        -- AND u.active = 'true' 
        AND p.patientStatus != 'Discharged'
        GROUP BY r.uid
        ORDER BY p.lastName, p.firstName, u.lastname, u.firstname";
    }
    $result = mysqli_query($link, $query);
    $jsonResult = "{\"users\": [";
    while($row = mysqli_fetch_assoc($result)){
        $jsonResult .= '{"uid": "'.$row['uid'].'",
        "userFirstName": "'.$row['firstname'].'",
        "userLastName": "'.$row['lastname'].'",
        "phone": "'.$row['phone'].'",
        "email": "'.$row['id'].'",
        "active": "'.$row['active'].'",
        "type": "'.$row['type'].'",
        "preferredContactType": "'.$row['contacttype'].'",
        "lastLogin": "'.$row['currentlogin'].'",
        "loginCount": "'.$row['logincount'].'",
        "patientFirstName": "'.$row['patientFirst'].'",
        "patientLastName": "'.$row['patientLast'].'",
        "admissionDate": "'.$row['admissionDate'].'",
        "dischargeDate": "'.$row['dischargeDate'].'",
        "orgUuid": "'.$row['orgUuid'].'",
        "patientId": "'.$row['patientId'].'"'.'},';
    }
    $jsonResult = substr($jsonResult, 0 , -1)."]}";
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
if($authRow['type'] == 'admin'){
    $query = "SELECT p.firstName as patientFirst, p.lastName as patientLast, 
        u.uid, u.phone, u.lastname, u.firstname, u.active, u.type, u.id, u.uid, u.currentlogin, u.logincount, u.contacttype,
        r.facId, r.orgUuid, p.patientId, p.patientStatus, p.admissionDate, p.dischargeDate 
        FROM relations r
        JOIN user u 
        ON r.uid = u.uid
        JOIN patients p 
        ON p.patientId = r.patient
        WHERE r.orgUuid = '$orgUuid'
        AND r.facId = '$facId'
        AND u.active = 'true' 
        AND p.patientStatus != 'Discharged'
        GROUP BY r.uid
        ORDER BY p.lastName, p.firstName, u.lastname, u.firstname";
    $result = mysqli_query($link, $query);
    $jsonResult = "{\"users\": [";
        while($row = mysqli_fetch_assoc($result)){
            $jsonResult .= '{"uid": "'.$row['uid'].'",
            "userFirstName": "'.$row['firstname'].'",
            "userLastName": "'.$row['lastname'].'",
            "phone": "'.$row['phone'].'",
            "email": "'.$row['id'].'",
            "active": "'.$row['active'].'",
            "type": "'.$row['type'].'",
            "preferredContactType": "'.$row['contacttype'].'",
            "lastLogin": "'.$row['currentlogin'].'",
            "loginCount": "'.$row['logincount'].'",
            "patientFirstName": "'.$row['patientFirst'].'",
            "patientLastName": "'.$row['patientLast'].'",
            "admissionDate": "'.$row['admissionDate'].'",
            "dischargeDate": "'.$row['dischargeDate'].'",
            "orgUuid": "'.$row['orgUuid'].'",
            "patientId": "'.$row['patientId'].'"'.'},';
        }
    $jsonResult = substr($jsonResult, 0 , -1)."]}";
    http_response_code(200);
    header('Content-Type: application/json');
    exit($jsonResult);
}
else {
    http_response_code(200);
    header('Content-Type: application/json');
    exit("{\"data\": {}");
}